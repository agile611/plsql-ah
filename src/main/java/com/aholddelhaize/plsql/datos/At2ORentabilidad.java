package com.aholddelhaize.plsql.datos;


import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.jdbc.OracleTypes;
import oracle.jpub.runtime.MutableStruct;
import oracle.sql.Datum;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.sql.STRUCT;

/**
 * @author guillem.hernandez
 */
public class At2ORentabilidad implements ORAData, ORADataFactory {
    public static final String _SQL_NAME = "AT2_O_RENTABILIDAD";
    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    protected MutableStruct _struct;

    protected static int[] _sqlType = {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};
    protected static ORADataFactory[] _factory = new ORADataFactory[40];
    protected static final At2ORentabilidad _At2ORentabilidadFactory = new At2ORentabilidad();


    public static ORADataFactory getORADataFactory() {
        return _At2ORentabilidadFactory;
    }

    protected void _init_struct(boolean init) {
        if (init) {
            _struct = new MutableStruct(new Object[15], _sqlType, _factory);
        }
    }

    /* ORAData interface */
    @Override
    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, _SQL_NAME);
    }

    public At2ORentabilidad() {
        _init_struct(true);
    }

    @Override
    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(OReFuCambioDivisaGen o, Datum d, int sqlType) throws SQLException {
        if (d == null) {
            return null;
        }
        if (o == null) {
            o = new OReFuCambioDivisaGen();
        }
        o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
        return o;
    }

    public At2ORentabilidad(
            String totPvp,
            String espPvp,
            String genPvp,
            String pvpFac,
            String totCos,
            String espCos,
            String genCos,
            String cosFac,
            String comAge,
            String impAge,
            String comRap,
            String impRap,
            String comOvv,
            String impOvv,
            String comHot,
            String impHot,
            String comPar,
            String impPar,
            String comMak,
            String impMak,
            String comBan,
            String impBan,
            String totRed,
            String totBod,
            String totBot,
            String impBot,
            String totMar,
            String impMar,
            String pctRen,
            String totPvpPar,
            String comAgePar,
            String impAgePar,
            String comPfe,
            String impPfe,
            String totcosdstab,
            String cosfacdstab,
            String gencosdstab,
            String espcosdstab,
            String ImpTrf,
            String comtarj,
            String imptarj,
            String locallevy,
            String comsc,
            String impsc,
            String implocallevy
    ) {
        try {
            _init_struct(true);
            setTotPvp(totPvp);
            setEspPvp(espPvp);
            setGenPvp(genPvp);
            setPvpFac(pvpFac);
            setTotCos(totCos);
            setEstCos(espCos);
            setGenCos(genCos);
            setCosFac(cosFac);
            setComAge(comAge);
            setImpAge(impAge);
            setComRap(comRap);
            setImpRap(impRap);
            setComOvv(comOvv);
            setImpOvv(impOvv);
            setComHot(comHot);
            setImpHot(impHot);
            setComPar(comPar);
            setImpPar(impPar);
            setComMak(comMak);
            setImpMak(impMak);
            setComBan(comBan);
            setImpBan(impBan);
            setTotRed(totRed);
            setTotBod(totBod);
            setTotBot(totBot);
            setImpBot(impBot);
            setTotMar(totMar);
            setImpMar(impMar);
            setPctRen(pctRen);
            setTotPvpPar(totPvpPar);
            setComAgePar(comAgePar);
            setImpAgePar(impAgePar);
            setComPfe(comPfe);
            setImpPfe(impPfe);
            setTotCosdsTab(totcosdstab);
            setCosFacdsTab(cosfacdstab);
            setGencosdsTab(gencosdstab);
            setEspcosdsTab(espcosdstab);
            setImpTrf(ImpTrf);
            setComTarj(comtarj);
            setImpTarj(imptarj);
            setLocalLevy(locallevy);
            setComsc(comsc);
            setImpsc(impsc);
            setImpLocalLevy(implocallevy);
        } catch (SQLException ex) {
            Logger.getLogger(OReFuCambioDivisaGen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setTotPvp(String totPvp) throws SQLException {
        _struct.setAttribute(0, totPvp);
    }

    public void setEspPvp(String espPvp) throws SQLException {
        _struct.setAttribute(1, espPvp);
    }

    public void setGenPvp(String genPvp) throws SQLException {
        _struct.setAttribute(2, genPvp);
    }

    public void setPvpFac(String pvpFac) throws SQLException {
        _struct.setAttribute(3, pvpFac);
    }

    public void setTotCos(String totCos) throws SQLException {
        _struct.setAttribute(4, totCos);
    }

    public void setEstCos(String espCos) throws SQLException {
        _struct.setAttribute(5, espCos);
    }

    public void setGenCos(String genCos) throws SQLException {
        _struct.setAttribute(6, genCos);
    }

    public void setCosFac(String cosFac) throws SQLException {
        _struct.setAttribute(7, cosFac);
    }

    public void setComAge(String comAge) throws SQLException {
        _struct.setAttribute(8, comAge);
    }

    public void setImpAge(String impAge) throws SQLException {
        _struct.setAttribute(9, impAge);
    }

    public void setComRap(String comRap) throws SQLException {
        _struct.setAttribute(10, comRap);
    }

    public void setImpRap(String impRap) throws SQLException {
        _struct.setAttribute(11, impRap);
    }

    public void setComOvv(String comOvv) throws SQLException {
        _struct.setAttribute(12, comOvv);
    }

    public void setImpOvv(String impOvv) throws SQLException {
        _struct.setAttribute(13, impOvv);
    }

    public void setComHot(String comHot) throws SQLException {
        _struct.setAttribute(14, comHot);
    }

    public void setImpHot(String impHot) throws SQLException {
        _struct.setAttribute(15, impHot);
    }

    public void setComPar(String comPar) throws SQLException {
        _struct.setAttribute(16, comPar);
    }

    public void setImpPar(String impPar) throws SQLException {
        _struct.setAttribute(17, impPar);
    }

    public void setComMak(String comMak) throws SQLException {
        _struct.setAttribute(18, comMak);
    }

    public void setImpMak(String impMak) throws SQLException {
        _struct.setAttribute(19, impMak);
    }

    public void setComBan(String comBan) throws SQLException {
        _struct.setAttribute(20, comBan);
    }

    public void setImpBan(String impBan) throws SQLException {
        _struct.setAttribute(21, impBan);
    }

    public void setTotRed(String totRed) throws SQLException {
        _struct.setAttribute(22, totRed);
    }

    public void setTotBod(String totBod) throws SQLException {
        _struct.setAttribute(23, totBod);
    }

    public void setTotBot(String totBot) throws SQLException {
        _struct.setAttribute(24, totBot);
    }

    public void setImpBot(String impBot) throws SQLException {
        _struct.setAttribute(25, impBot);
    }

    public void setTotMar(String totMar) throws SQLException {
        _struct.setAttribute(26, totMar);
    }

    public void setImpMar(String impMar) throws SQLException {
        _struct.setAttribute(27, impMar);
    }

    public void setPctRen(String pctRen) throws SQLException {
        _struct.setAttribute(28, pctRen);
    }

    public void setTotPvpPar(String totPvpPar) throws SQLException {
        _struct.setAttribute(29, totPvpPar);
    }

    public void setComAgePar(String comAgePar) throws SQLException {
        _struct.setAttribute(30, comAgePar);
    }

    public void setImpAgePar(String impAgePar) throws SQLException {
        _struct.setAttribute(31, impAgePar);
    }

    public void setComPfe(String comPfe) throws SQLException {
        _struct.setAttribute(32, comPfe);
    }

    public void setImpPfe(String impPfe) throws SQLException {
        _struct.setAttribute(33, impPfe);
    }

    public void setTotCosdsTab(String totCosdsTab) throws SQLException {
        _struct.setAttribute(34, totCosdsTab);
    }

    public void setCosFacdsTab(String cosfacdstab) throws SQLException {
        _struct.setAttribute(35, cosfacdstab);
    }

    public void setGencosdsTab(String gencosdstab) throws SQLException {
        _struct.setAttribute(36, gencosdstab);
    }


    public void setEspcosdsTab(String espcosdstab) throws SQLException {
        _struct.setAttribute(37, espcosdstab);
    }

    public void setImpTrf(String ImpTrf) throws SQLException {
        _struct.setAttribute(38, ImpTrf);
    }

    public void setComTarj(String comtarj) throws SQLException {
        _struct.setAttribute(39, comtarj);
    }

    public void setImpTarj(String imptarj) throws SQLException {
        _struct.setAttribute(40, imptarj);
    }

    public void setLocalLevy(String locallevy) throws SQLException {
        _struct.setAttribute(41, locallevy);
    }

    public void setComsc(String comsc) throws SQLException {
        _struct.setAttribute(42, comsc);
    }

    public void setImpsc(String impsc) throws SQLException {
        _struct.setAttribute(43, impsc);
    }

    public void setImpLocalLevy(String implocallevy) throws SQLException {
        _struct.setAttribute(44, implocallevy);
    }

    public String getTotPvp() throws SQLException {
        return _struct.getOracleAttribute(0).toString();
    }

    public String getEspPvp() throws SQLException {
        return _struct.getOracleAttribute(1).toString();
    }

    public String getGenPvp() throws SQLException {
        return _struct.getOracleAttribute(2).toString();
    }

    public String getPvpFac() throws SQLException {
        return _struct.getOracleAttribute(3).toString();
    }

    public String getTotCos() throws SQLException {
        return _struct.getOracleAttribute(4).toString();
    }

    public String getEspCos() throws SQLException {
        return _struct.getOracleAttribute(5).toString();
    }

    public String getGenCos() throws SQLException {
        return _struct.getOracleAttribute(6).toString();
    }

    public String getCosFac() throws SQLException {
        return _struct.getOracleAttribute(7).toString();
    }

    public String getComAge() throws SQLException {
        return _struct.getOracleAttribute(8).toString();
    }

    public String getImpAge() throws SQLException {
        return _struct.getOracleAttribute(9).toString();
    }

    public String getComRap() throws SQLException {
        return _struct.getOracleAttribute(10).toString();
    }

    public String getImpRap() throws SQLException {
        return _struct.getOracleAttribute(11).toString();
    }

    public String getComOvv() throws SQLException {
        return _struct.getOracleAttribute(12).toString();
    }

    public String getImpOvv() throws SQLException {
        return _struct.getOracleAttribute(13).toString();
    }

    public String getComHot() throws SQLException {
        return _struct.getOracleAttribute(14).toString();
    }

    public String getImpHot() throws SQLException {
        return _struct.getOracleAttribute(15).toString();
    }

    public String getComPar() throws SQLException {
        return _struct.getOracleAttribute(16).toString();
    }

    public String getImpPar() throws SQLException {
        return _struct.getOracleAttribute(17).toString();
    }

    public String getComMak() throws SQLException {
        return _struct.getOracleAttribute(18).toString();
    }

    public String getImpMak() throws SQLException {
        return _struct.getOracleAttribute(19).toString();
    }

    public String getComBan() throws SQLException {
        return _struct.getOracleAttribute(20).toString();
    }

    public String getImpBan() throws SQLException {
        return _struct.getOracleAttribute(21).toString();
    }

    public String getTotRed() throws SQLException {
        return _struct.getOracleAttribute(22).toString();
    }

    public String getTotBod() throws SQLException {
        return _struct.getOracleAttribute(23).toString();
    }

    public String getTotBot() throws SQLException {
        return _struct.getOracleAttribute(24).toString();
    }

    public String getImpBot() throws SQLException {
        return _struct.getOracleAttribute(25).toString();
    }

    public String getTotMar() throws SQLException {
        return _struct.getOracleAttribute(26).toString();
    }

    public String getImpMar() throws SQLException {
        return _struct.getOracleAttribute(27).toString();
    }

    public String getPctRen() throws SQLException {
        return _struct.getOracleAttribute(28).toString();
    }

    public String getTotPvpPar() throws SQLException {
        return _struct.getOracleAttribute(29).toString();
    }

    public String getComAgePar() throws SQLException {
        return _struct.getOracleAttribute(30).toString();
    }

    public String getImpAgePar() throws SQLException {
        return _struct.getOracleAttribute(31).toString();
    }

    public String getComPfe() throws SQLException {
        return _struct.getOracleAttribute(32).toString();
    }

    public String getImpPfe() throws SQLException {
        return _struct.getOracleAttribute(33).toString();
    }

    public String getTotCosdsTab() throws SQLException {
        return _struct.getOracleAttribute(34).toString();
    }

    public String getCosFacdsTab() throws SQLException {
        return _struct.getOracleAttribute(35).toString();
    }

    public String getGencosdsTab() throws SQLException {
        return _struct.getOracleAttribute(36).toString();
    }

    public String getEspcosdsTab() throws SQLException {
        return _struct.getOracleAttribute(37).toString();
    }

    public String getImpTrf(String ImpTrf) throws SQLException {
        return _struct.getOracleAttribute(38).toString();
    }

    public String getComTarj() throws SQLException {
        return _struct.getOracleAttribute(39).toString();
    }

    public String getImpTarj() throws SQLException {
        return _struct.getOracleAttribute(40).toString();
    }

    public String getLocalLevy() throws SQLException {
        return _struct.getOracleAttribute(41).toString();
    }

    public String getComsc() throws SQLException {
        return _struct.getOracleAttribute(42).toString();
    }

    public String getImpsc() throws SQLException {
        return _struct.getOracleAttribute(43).toString();
    }

    public String getImpLocalLevy() throws SQLException {
        return _struct.getOracleAttribute(44).toString();
    }
}
