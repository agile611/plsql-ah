/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aholddelhaize.plsql.datos;

/**
 * @author guillem.hernandez
 */

import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;
import oracle.jpub.runtime.MutableArray;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.Datum;
import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;

public class OTReFuCambioDivisaGen implements ORAData, ORADataFactory {
    public static final String _SQL_NAME = "OT_RE_FU_CAMBIO_DIVISA_GEN";
    public static final int _SQL_TYPECODE = OracleTypes.ARRAY;

    MutableArray _array;

    private static final OTReFuCambioDivisaGen _OTReFuCambioDivisaGenFactory = new OTReFuCambioDivisaGen();

    public static ORADataFactory getORADataFactory() {
        return _OTReFuCambioDivisaGenFactory;
    }

    /* constructors */
    public OTReFuCambioDivisaGen() {
        this((OReFuCambioDivisaGen[]) null);
    }

    public OTReFuCambioDivisaGen(OReFuCambioDivisaGen[] a) {
        _array = new MutableArray(2002, a, OReFuCambioDivisaGen.getORADataFactory());
    }

    /* ORAData interface */
    @Override
    public Datum toDatum(Connection c) throws SQLException {
        if (__schemaName != null) {
            return _array.toDatum(c, __schemaName + "." + _SQL_NAME);
        }
        return _array.toDatum(c, _SQL_NAME);
    }

    private String __schemaName = null;

    public void __setSchemaName(String schemaName) {
        __schemaName = schemaName;
    }

    /* ORADataFactory interface */
    @Override
    public ORAData create(Datum d, int sqlType) throws SQLException {
        if (d == null) {
            return null;
        }
        OTReFuCambioDivisaGen a = new OTReFuCambioDivisaGen();
        a._array = new MutableArray(2002, (ARRAY) d, OReFuCambioDivisaGen.getORADataFactory());
        return a;
    }

    public int length() throws SQLException {
        return _array.length();
    }

    public int getBaseType() throws SQLException {
        return _array.getBaseType();
    }

    public String getBaseTypeName() throws SQLException {
        return _array.getBaseTypeName();
    }

    public ArrayDescriptor getDescriptor() throws SQLException {
        return _array.getDescriptor();
    }

    /* array accessor methods */
    public OReFuCambioDivisaGen[] getArray() throws SQLException {
        return (OReFuCambioDivisaGen[]) _array.getObjectArray(
                new OReFuCambioDivisaGen[_array.length()]
        );
    }

    public OReFuCambioDivisaGen[] getArray(long index, int count) throws SQLException {
        return (OReFuCambioDivisaGen[]) _array.getObjectArray(
                index,
                new OReFuCambioDivisaGen[_array.sliceLength(index, count)]
        );
    }

    public void setArray(OReFuCambioDivisaGen[] a) throws SQLException {
        _array.setObjectArray(a);
    }

    public void setArray(OReFuCambioDivisaGen[] a, long index) throws SQLException {
        _array.setObjectArray(a, index);
    }

    public OReFuCambioDivisaGen getElement(long index) throws SQLException {
        return (OReFuCambioDivisaGen) _array.getObjectElement(index);
    }

    public void setElement(OReFuCambioDivisaGen a, long index) throws SQLException {
        _array.setObjectElement(a, index);
    }
}
