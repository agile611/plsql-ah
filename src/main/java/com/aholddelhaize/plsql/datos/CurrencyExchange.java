/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aholddelhaize.plsql.datos;

/**
 * @author guillem.hernandez
 */
public class CurrencyExchange {
    String checkin;
    String clientId;
    String companyId;
    String creationDate;
    String dateExchangeType;
    String destinationCurrencyId;
    String exchangeDirect;
    String exchangeType;
    String partnerId;
    String sourceCurrencyId;
    String spreadType;

    public CurrencyExchange() {
    }

    public CurrencyExchange(
            String companyId,
            String exchangeType,
            String creationDate,
            String checkin,
            String dateExchangeType,
            String clientId,
            String sourceCurrencyId,
            String destinationCurrencyId,
            String partnerId,
            String exchangeDirect,
            String spreadType
    ) {
        this.checkin = checkin;
        this.clientId = clientId;
        this.companyId = companyId;
        this.creationDate = creationDate;
        this.dateExchangeType = dateExchangeType;
        this.destinationCurrencyId = destinationCurrencyId;
        this.exchangeDirect = exchangeDirect;
        this.exchangeType = exchangeType;
        this.partnerId = partnerId;
        this.sourceCurrencyId = sourceCurrencyId;
        this.spreadType = spreadType;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDateExchangeType() {
        return dateExchangeType;
    }

    public void setDateExchangeType(String dateExchangeType) {
        this.dateExchangeType = dateExchangeType;
    }

    public String getDestinationCurrencyId() {
        return destinationCurrencyId;
    }

    public void setDestinationCurrencyId(String destinationCurrencyId) {
        this.destinationCurrencyId = destinationCurrencyId;
    }

    public String getExchangeDirect() {
        return exchangeDirect;
    }

    public void setExchangeDirect(String exchangeDirect) {
        this.exchangeDirect = exchangeDirect;
    }

    public String getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(String exchangeType) {
        this.exchangeType = exchangeType;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSourceCurrencyId() {
        return sourceCurrencyId;
    }

    public void setSourceCurrencyId(String sourceCurrencyId) {
        this.sourceCurrencyId = sourceCurrencyId;
    }

    public String getSpreadType() {
        return spreadType;
    }

    public void setSpreadType(String spreadType) {
        this.spreadType = spreadType;
    }

}
