/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aholddelhaize.plsql.datos;

/**
 * @author guillem.hernandez
 */
public class CurrencyExchangeResult {
    String rate;
    String decimalPlaces;

    public CurrencyExchangeResult(String rate, String decimalPlaces) {
        this.rate = rate;
        this.decimalPlaces = decimalPlaces;
    }

    public CurrencyExchangeResult() {
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDecimalPlaces() {
        return decimalPlaces;
    }

    public void setDecimalPlaces(String decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

}
