package com.aholddelhaize.plsql.datos;

import lombok.Data;

/**
 * @author guillem.hernandez
 */

public
@Data
class Reserva {
    private String seq_rec;
    private String seq_reserva;
    private String tipPag;
    private String empFac;
    private String codDiv;
    private String fecCam;
    private String codInterface;

    public Reserva() {
    }

    public String getSeq_rec() {
        return seq_rec;
    }

    public void setSeq_rec(String seq_rec) {
        this.seq_rec = seq_rec;
    }

    public String getSeq_reserva() {
        return seq_reserva;
    }

    public void setSeq_reserva(String rate) {
        this.seq_reserva = seq_reserva;
    }
}
