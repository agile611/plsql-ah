/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aholddelhaize.plsql.datos;

import java.sql.Connection;

import oracle.sql.ORAData;
import oracle.sql.ORADataFactory;
import oracle.jdbc.OracleTypes;
import oracle.jpub.runtime.MutableStruct;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import oracle.sql.Datum;
import oracle.sql.STRUCT;

/**
 * @author guillem.hernandez
 */
public class OReFuCambioDivisaGen implements ORAData, ORADataFactory {

    public static final String _SQL_NAME = "O_RE_FU_CAMBIO_DIVISA_GEN";
    public static final int _SQL_TYPECODE = OracleTypes.STRUCT;

    protected MutableStruct _struct;

    protected static int[] _sqlType = {12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};
    protected static ORADataFactory[] _factory = new ORADataFactory[40];
    protected static final OReFuCambioDivisaGen _OReFuCambioDivisaGenFactory = new OReFuCambioDivisaGen();

    public static ORADataFactory getORADataFactory() {
        return _OReFuCambioDivisaGenFactory;
    }

    protected void _init_struct(boolean init) {
        if (init) {
            _struct = new MutableStruct(new Object[15], _sqlType, _factory);
        }
    }

    /* ORAData interface */
    @Override
    public Datum toDatum(Connection c) throws SQLException {
        return _struct.toDatum(c, _SQL_NAME);
    }

    public OReFuCambioDivisaGen() {
        _init_struct(true);
    }

    @Override
    public ORAData create(Datum d, int sqlType) throws SQLException {
        return create(null, d, sqlType);
    }

    protected ORAData create(OReFuCambioDivisaGen o, Datum d, int sqlType) throws SQLException {
        if (d == null) {
            return null;
        }
        if (o == null) {
            o = new OReFuCambioDivisaGen();
        }
        o._struct = new MutableStruct((STRUCT) d, _sqlType, _factory);
        return o;
    }

    public OReFuCambioDivisaGen(
            String empresa,
            String tipoCambio,
            String fechaCreacion,
            String fechaServicio,
            String indFecCambio,
            String TTOO,
            String divOrigen,
            String divDestino,
            String importe,
            String redondear,
            String partner,
            String cambioDirecto,
            String spread,
            String resultado,
            String desError,
            String decimales
    ) {
        try {
            _init_struct(true);
            setEmpresa(empresa);
            setTipoCambio(tipoCambio);
            setFechaCreacion(fechaCreacion);
            setFechaServicio(fechaServicio);
            setIndFecCambio(indFecCambio);
            setTTOO(TTOO);
            setDivOrigen(divOrigen);
            setDivDestino(divDestino);
            setImporte(importe);
            setRedondear(redondear);
            setPartner(partner);
            setCambioDirecto(cambioDirecto);
            setSpread(spread);
            setResultado(resultado);
            setDesError(desError);
            setDecimales(decimales);
        } catch (SQLException ex) {
            Logger.getLogger(OReFuCambioDivisaGen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void setEmpresa(String empresa) throws SQLException {
        _struct.setAttribute(0, empresa);
    }

    public void setTipoCambio(String tipoCambio) throws SQLException {
        _struct.setAttribute(1, tipoCambio);
    }

    public void setFechaCreacion(String fechaCreacion) throws SQLException {
        _struct.setAttribute(2, fechaCreacion);
    }

    public void setFechaServicio(String fechaServicio) throws SQLException {
        _struct.setAttribute(3, fechaServicio);
    }

    public void setIndFecCambio(String indFecCambio) throws SQLException {
        _struct.setAttribute(4, indFecCambio);
    }

    public void setTTOO(String TTOO) throws SQLException {
        _struct.setAttribute(5, TTOO);
    }

    public void setDivOrigen(String divOrigen) throws SQLException {
        _struct.setAttribute(6, divOrigen);
    }

    public void setDivDestino(String divDestino) throws SQLException {
        _struct.setAttribute(7, divDestino);
    }

    public void setImporte(String importe) throws SQLException {
        _struct.setAttribute(8, importe);
    }

    public void setRedondear(String redondear) throws SQLException {
        _struct.setAttribute(9, redondear);
    }

    public void setPartner(String partner) throws SQLException {
        _struct.setAttribute(10, partner);
    }

    public void setCambioDirecto(String cambioDirecto) throws SQLException {
        _struct.setAttribute(11, cambioDirecto);
    }

    public void setSpread(String spread) throws SQLException {
        _struct.setAttribute(12, spread);
    }

    public void setResultado(String resultado) throws SQLException {
        _struct.setAttribute(13, resultado);
    }

    public void setDesError(String desError) throws SQLException {
        _struct.setAttribute(14, desError);
    }

    public void setDecimales(String decimales) throws SQLException {
        _struct.setAttribute(15, decimales);
    }

    public String getEmpresa() throws SQLException {
        return _struct.getOracleAttribute(0).toString();
    }

    public String getTipoCambio() throws SQLException {
        return (String) _struct.getOracleAttribute(1).toString();
    }

    public String getFechaCreacion() throws SQLException {
        return _struct.getOracleAttribute(2).toString();
    }

    public String getFechaServicio() throws SQLException {
        return _struct.getOracleAttribute(3).toString();
        /*
        String S = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(_struct.getOracleAttribute(3));
        return S; 
*/
    }

    public String getIndFecCambio() throws SQLException {
        return _struct.getOracleAttribute(4).toString();
    }

    public String getTTOO() throws SQLException {
        return _struct.getOracleAttribute(5).toString();
    }

    public String getDivOrigen() throws SQLException {
        return _struct.getOracleAttribute(6).toString();
    }

    public String getDivDestino() throws SQLException {
        return _struct.getOracleAttribute(7).toString();
    }

    public String getImporte() throws SQLException {
        return _struct.getOracleAttribute(8).toString();
    }

    public String getRedondear() throws SQLException {
        return _struct.getOracleAttribute(9).toString();
    }

    public String getPartner() throws SQLException {
        if (_struct.getOracleAttribute(10) == null) {
            return "null";
        }
        return _struct.getOracleAttribute(10).toString();
    }

    public String getCambioDirecto() throws SQLException {
        return _struct.getOracleAttribute(11).toString();
    }

    public String getSpread() throws SQLException {
        return _struct.getOracleAttribute(12).toString();
    }

    public String getResultado() throws SQLException {
        if (_struct.getOracleAttribute(13) == null) {
            return null;
        } else {
            return _struct.getOracleAttribute(13).toString();
        }
    }

    public String getDesError() throws SQLException {
        if (_struct.getOracleAttribute(14) == null) {
            return null;
        } else {
            return _struct.getOracleAttribute(14).toString();
        }
    }

    public String getDecimales() throws SQLException {
        return _struct.getOracleAttribute(15).toString();
    }

}
