package com.aholddelhaize.plsql.datos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author guillem.hernandez
 */
public class CostesReserva {
    public static Reserva[] comparaCostes(Connection con, String dbName, int size) throws SQLException {
        Statement stmt = null;
        Reserva[] resArray = new Reserva[size];
        Reserva res = new Reserva();
        int i = 0;
        String query = "    select  grec_seq_rec, rres_seq_reserva\n" +
                "    from    re_t_re_traslado\n" +
                "    where   fec_traslado    >   sysdate\n" +
                "     and    fec_cancelacion is  null\n" +
                "     and    fec_creacion    >   sysdate - 60\n" +
                "     and    (grec_seq_rec, rres_seq_reserva) in (\n" +
                "               select  grec_seq_rec, rres_seq_reserva\n" +
                "               from    re_t_re_hotel_venta\n" +
                "               where   fec_desde           >   sysdate \n" +
                "                and    fec_cancelacion     is  null\n" +
                "                and    fec_creacion        >   sysdate - 60\n" +
                "               )\n" +
                "                and    (grec_seq_rec, rres_seq_reserva) in ( \n" +
                "                select  seq_rec, seq_reserva\n" +
                "                from    re_t_re_otro\n" +
                "                where   fec_cancelacion     is  null\n" +
                "                 and    fec_desde           >   sysdate\n" +
                "                 and    fec_creacion        >   sysdate - 60\n" +
                "   )";
        try {
            stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next() && i < size) {
                String receptivo = rs.getString("grec_seq_rec");
                String reserva = rs.getString("rres_seq_reserva");
                res.setSeq_rec(receptivo);
                res.setSeq_reserva(reserva);
                resArray[i] = res;
                i++;
                System.out.println(receptivo + "-" + reserva + "-" + i);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
        }
        return resArray;
    }
}
