package com.aholddelhaize.plsql;

import com.aholddelhaize.plsql.datos.At2ORentabilidad;
import com.aholddelhaize.plsql.datos.CostesReserva;
import com.aholddelhaize.plsql.datos.Reserva;
//import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleCallableStatement;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author ghernandez
 */
public class PruebaTest {
    @Test
    public void testingConnection() throws IOException, SQLException, ClassNotFoundException {
        try {
            CostesReserva cR = new CostesReserva();
            Class.forName("oracle.jdbc.driver.OracleDriver");
            Connection conexion = DriverManager.getConnection("jdbc:oracle:thin:@//atlas-db.service.test.discovery:1666/atltest", "RECEP", "RECEP");
            Reserva[] res = new Reserva[500];
            res = cR.comparaCostes(conexion, "RECEP", res.length);
            for (Reserva r : res) {
                OracleCallableStatement ocs = (OracleCallableStatement) conexion.prepareCall("{call Re_pk_rentabilidad.ObtenerCostesReserva (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                /*PROCEDURE ObtenerCostesReserva(p_seqrec NUMBER,
                        p_seqres NUMBER,
                        p_tippag VARCHAR2,
                        p_empfac VARCHAR2,
                        p_divres VARCHAR2,
                        p_feccam DATE,
                        p_datren IN OUT NOCOPY R_RENTABILIDAD,
                p_regexc VARCHAR2 DEFAULT NULL,
                p_coddiv VARCHAR2 DEFAULT NULL,
                p_tipser VARCHAR2 DEFAULT NULL,
                p_ordser NUMBER DEFAULT NULL,
                p_indfac VARCHAR2 DEFAULT 'N',     -- Forzamos a la rentabilidad a mostrar costes no facturables
                p_ordser_com NUMBER DEFAULT NULL); --ATLAS-28537 - Orden del servicio de compra*/
                ocs.setString(1, r.getSeq_rec()); //p_seqrec
                ocs.setString(2, r.getSeq_reserva()); //p_seqres
                ocs.setString(3, "P"); //p_tippag
                ocs.setString(4, "E14"); //p_empfac
                ocs.setString(5, "EUR"); //p_divres
                ocs.setString(6, "08-MAR-17"); //p_feccam
                ocs.registerOutParameter(7, At2ORentabilidad._SQL_TYPECODE, At2ORentabilidad._SQL_NAME);//p_datren
                ocs.setString(8, null); //p_regexc VARCHAR2 DEFAULT NULL,
                ocs.setString(9, null); //p_coddiv VARCHAR2 DEFAULT NULL,
                ocs.setString(10, null); //p_tipser VARCHAR2 DEFAULT NULL,
                ocs.setString(11, null); //p_ordser NUMBER DEFAULT NULL,
                ocs.setString(12, "N"); //p_indfac VARCHAR2 DEFAULT 'N',     -- Forzamos a la rentabilidad a mostrar costes no facturables
                ocs.setString(13, null); //p_ordser_com NUMBER DEFAULT NULL
                ocs.execute();
            }
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Error en la conexión de la base de datos");
            System.out.println(ex.getMessage());
        }
    }
}
